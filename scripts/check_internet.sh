#!/bin/bash
echo "[...] Checking internet connexion[...]"
ping -c5 8.8.8.8 >> /dev/null

if [ $? -ne 0 ]
	then
		echo "[...] Internet access NOK [...]"
	else
		echo "[...] Internet access OK [...]"
fi
